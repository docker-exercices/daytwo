from fastapi import FastAPI, HTTPException
import asyncio
import httpx
from fastapi.responses import JSONResponse

server3_address = "http://server3:8080"

app2 = FastAPI()

# Endpoint pour s'enregistrer auprès du serveur intermédiaire
@app2.on_event("startup")
async def startup_event():
    async with httpx.AsyncClient() as client:
        response = await client.post(f"{server3_address}/register/server2", json={"server_address": "http://server2:5372"})
        response.raise_for_status()


# Adresse du premier serveur
server1_address = None
server4_address = None

@app2.get("/")
async def hello():
    return "serveur2"

# Endpoint pour recevoir "ping" et renvoyer "pong"
@app2.get("/pong")
async def pong():

    global server1_address
    global server4_address

    # Si l'adresse du premier serveur n'est pas encore connue, la demander au serveur intermédiaire
    if server1_address is None:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{server3_address}/get_address/server1")
            response.raise_for_status()
            server1_address = response.json()["server_address"]

    if server4_address is None:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{server3_address}/get_address/server4")
            response.raise_for_status()
            server4_address = response.json()["server_address"]

    try:
        await asyncio.sleep(1)
        print("pong")
        # Envoi de la requête "pong" en réponse à "ping"
        async with httpx.AsyncClient() as client:
            response = await client.post(f"{server4_address}/transmit", json={"recipient": server1_address, "action": "ping"})
            response.raise_for_status()
            return {"message": "ok"}

    except httpx.HTTPError as e:
        return JSONResponse(status_code=500, content={"error": str(e)})

