from fastapi import FastAPI
from typing import Dict

app3 = FastAPI()

registered_servers: Dict[str, str] = {}

@app3.get("/")
async def hello():
    return "serveur3"

@app3.post("/register/{server_name}")
async def register_server(server_name: str, data: dict):
    registered_servers[server_name] = data["server_address"]
    print(f"Server {server_name} registered successfully at {data['server_address']}")
    return {"message": f"Server {server_name} registered successfully at {data['server_address']}"}

@app3.get("/get_address/{server_name}")
async def get_server_address(server_name: str):
    if server_name in registered_servers:
        return {"server_address": registered_servers[server_name]}
    else:
        return {"error": f"Server {server_name} not registered"}
